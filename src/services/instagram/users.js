import { client, getAccessToken } from './client';

export const getOwnerRecentMedia = ({ count = 6 } = {}) => {
  const params = {
    access_token: getAccessToken(),
    count,
  };

  return client
    .get('/users/self/media/recent', { params })
    .then(({ data }) => data);
};
