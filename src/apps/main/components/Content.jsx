import styled from 'styled-components';

const Content = styled.div`
  min-height: calc(100vh - 144px);
`;

export default Content;
