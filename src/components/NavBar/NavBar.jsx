import React from 'react';
import styled from 'styled-components';

import colors from 'theme/colors';
import { withPadding } from 'enhancers/withIndentation';

import BarsIcon from 'components/Icon/BarsIcon';
import TimesIcon from 'components/Icon/TimesIcon';
import Link from 'components/Link';

import Breakpoints from '../Grid/breakpoints';

const NavBarList = styled.ul`
  display: inline-flex;
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: ${colors.black};
`;

const NavBarItem = styled(withPadding()('li'))`
  text-align: center;
`;

const MenuToggle = ({ open, className, onToggle }) => (
  <Link className={className} onClick={onToggle}>
    {open ? <TimesIcon /> : <BarsIcon />}
  </Link>
);

const StyledMenuToggle = styled(withPadding()(MenuToggle))`
  display: none;
  
  @media only screen and (max-width: ${Breakpoints.XS}px) {
    display: block;
    
    & + ${NavBarList} {
      display: ${({ open }) => (open ? 'block' : 'none')};
      position: fixed;
      width: 100%;
      height: 100%;
      border-top: 1px solid ${colors.white};
      z-index: 100;
    }
  }
`;

StyledMenuToggle.defaultProps = {
  pa: 'h v-double',
};

NavBarItem.defaultProps = {
  pa: 'h v-double',
};

class NavBar extends React.Component {
  state = {
    open: false,
  };

  handleToggle = (e) => {
    e.preventDefault();

    this.setState({ open: !this.state.open });
  };

  componentDidUpdate() {
    this.toggleBodyScroll(this.state.open);
  }

  componentWillUnmount() {
    this.toggleBodyScroll(false);
  }

  toggleBodyScroll(open) {
    document.body.style.overflow = open ? 'hidden' : 'visible';
  }

  render() {
    const { open } = this.state;
    const { children } = this.props;

    return (
      <React.Fragment>
        <StyledMenuToggle open={open} onToggle={this.handleToggle} />
        <NavBarList open={open}>
          {children}
        </NavBarList>
      </React.Fragment>
    );
  }
}

NavBar.Item = NavBarItem;

export default NavBar;
