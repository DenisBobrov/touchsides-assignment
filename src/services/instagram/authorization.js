import queryString from 'query-string';

const INSTAGRAM_AUTH_URL = 'https://api.instagram.com/oauth/authorize/';

const CLIENT_ID = 'ec84b412ad224779849f2b1ace5f1c04';
const REDIRECT_URI = 'http://localhost:3000/instagram/token';

export const getAuthorizationUrl = (clientId = CLIENT_ID, redirectUri = REDIRECT_URI) => {
  const query = queryString.stringify({
    client_id: clientId,
    redirect_uri: redirectUri,
    response_type: 'token',
    scope: 'public_content',
  });

  return `${INSTAGRAM_AUTH_URL}?${query}`;
};
