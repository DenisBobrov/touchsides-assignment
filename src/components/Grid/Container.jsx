import styled from 'styled-components';

import FlexContainer from './FLexContainer';

export const MAX_COLUMNS_COUNT = 12;

const Container = styled(FlexContainer)`
  flex-wrap: wrap;
  width: 100%;
  flex-direction: ${({ direction }) => direction}
  align-items: ${({ alignItems }) => alignItems}
`;

Container.defaultProps = {
  direction: 'row',
  alignItems: 'flex-start',
};

export default Container;
