import styled from 'styled-components';

import Text from './Text';

const mapHeaderToFont = {
  h1: {
    size: 40,
    weight: 900,
  },
  h2: {
    size: 36,
    weight: 800,
  },
  h3: {
    size: 30,
    weight: 700,
  },
  h4: {
    size: 24,
    weight: 600,
  },
  h5: {
    size: 18,
    weight: 500,
  },
  h6: {
    size: 16,
    weight: 400,
  },
};

const normalizeHeader = header => (mapHeaderToFont[header] ? header : 'h1');

const Heading = styled(Text)`
  font-size: ${({ as }) => mapHeaderToFont[normalizeHeader(as)].size}px;
  font-weight: ${({ as }) => mapHeaderToFont[normalizeHeader(as)].weight};
  text-transform: uppercase;
`;

export default Heading;
