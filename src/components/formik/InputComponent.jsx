import React from 'react';

import { Input, FormGroup } from '../Form';

class InputComponent extends React.Component {
  render() {
    const { field, form: { touched, errors }, ...props } = this.props;

    const { name } = field;
    const error = touched[name] && errors[name];

    return (
      <FormGroup error={error}>
        <Input type="text" error={error} {...field} {...props} />
      </FormGroup>
    );
  }
}

InputComponent.defaultProps = {
  fullWidth: true,
};

export default InputComponent;
