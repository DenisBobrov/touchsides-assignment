export default {
  freshGreen: '#7bdb4c',
  bluishGrey: '#789199',
  gunmetal: '#555d60',
  warmGrey: '#8e8e8e',
  salmon: '#ff7676',
  black: '#000000',
  white: '#ffffff',
};
