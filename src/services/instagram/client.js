import axios from 'axios';

const STORAGE_KEY_INSTAGRAM_TOKEN = 'instagram.accessToken';
const URL_API_INSTAGRAM = 'https://api.instagram.com/v1';

export const client = axios.create({
  baseURL: URL_API_INSTAGRAM,
});

client.interceptors.response.use(null, (error) => {
  clearToken();

  return error;
});

const clearToken = () => localStorage.removeItem(STORAGE_KEY_INSTAGRAM_TOKEN);

export const storeAccessToken = accessToken => localStorage.setItem(STORAGE_KEY_INSTAGRAM_TOKEN, accessToken);

export const getAccessToken = () => localStorage.getItem(STORAGE_KEY_INSTAGRAM_TOKEN);
