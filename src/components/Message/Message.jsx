import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import CheckCircleIcon from 'components/Icon/CheckCircleIcon';
import ExclamationCircleIcon from 'components/Icon/ExclamationCircleIcon';
import Text from 'components/Text';

const MessageType = {
  SUCCESS: 'success',
  ERROR: 'error',
};

const messageTypeToIconMap = {
  [MessageType.SUCCESS]: CheckCircleIcon,
  [MessageType.ERROR]: ExclamationCircleIcon,
};

const getMessageIcon = (messageType, fallbackIcon = CheckCircleIcon) => messageTypeToIconMap[messageType] || fallbackIcon;

const MessageContent = styled.div`
  background-color: black;
  opacity: 0.7;
  padding: 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const IconWrapper = styled.div`
  display: flex;
  align-self: baseline;
  margin-right: 15px;
`;

class Message extends React.Component {
  render() {
    const { value, type } = this.props;
    const MessageIcon = getMessageIcon(type);

    return (
      <MessageContent>
        <IconWrapper>
          <MessageIcon inverse />
        </IconWrapper>
        <Text as="div" align="justify" fullWidth>{value}</Text>
      </MessageContent>
    );
  }
}

Message.propTypes = {
  value: PropTypes.string.isRequired,
  type: PropTypes.string,
};

Message.defaultProps = {
  type: MessageType.SUCCESS,
};

export default Message;
