import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NavBar from 'components/NavBar';
import { NavLink } from 'components/Link';

import { homeUrl, contactUrl, instagramAccessToken } from './urls';
import HomePage from './pages/HomePage';
import ContactPage from './pages/ContactPage';
import InstagramAccessTokenPage from './pages/InstagramAccessTokenPage';
import NotFoundPage from './pages/NotFoundPage';

import Header from './components/Header';
import Content from './components/Content';
import Footer from './components/Footer';

export default class MainApp extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header>
          <NavBar>
            <NavBar.Item><NavLink to={homeUrl()}>Home</NavLink></NavBar.Item>
            <NavBar.Item><NavLink to={contactUrl()}>Contact</NavLink></NavBar.Item>
          </NavBar>
        </Header>
        <Content>
          <Switch>
            <Route path={homeUrl()} component={HomePage} />
            <Route path={contactUrl()} component={ContactPage} />
            <Route path={instagramAccessToken()} component={InstagramAccessTokenPage} />
            <Route component={NotFoundPage} />
          </Switch>
        </Content>
        <Footer />
      </React.Fragment>
    );
  }
}
