import React from 'react';
import styled from 'styled-components';
import isString from 'lodash/isString';
import memoize from 'lodash/memoize';

const MARGIN = 10;
const PADDING = 10;
const RULES_SEPARATOR = ' ';
const VALUE_SEPARATOR = '-';

const getIndentationValue = (stringValue, baseValue) => {
  if (stringValue === 'half') {
    return baseValue / 2;
  }

  if (stringValue === 'double') {
    return baseValue * 2;
  }

  return baseValue;
};

const createIndentationRulesFunction = (ruleName, baseValue) => {
  const indentationPropertyToCssMap = {
    t: `${ruleName}-top`,
    b: `${ruleName}-bottom`,
    l: `${ruleName}-left`,
    r: `${ruleName}-right`,
  };

  return (indentation) => {
    if (indentation === true) {
      return `${ruleName}: ${MARGIN}px;`;
    }

    if (!isString(indentation)) {
      return '';
    }

    const rules = indentation.split(RULES_SEPARATOR);

    return rules.map((rule) => {
      const [indentation, value] = rule.split(VALUE_SEPARATOR);
      const indentationValue = getIndentationValue(value, baseValue);

      if (indentation === 'v') {
        return `
        ${indentationPropertyToCssMap.t}: ${indentationValue}px;
        ${indentationPropertyToCssMap.b}: ${indentationValue}px;
      `;
      }

      if (indentation === 'h') {
        return `
        ${indentationPropertyToCssMap.l}: ${indentationValue}px;
        ${indentationPropertyToCssMap.r}: ${indentationValue}px;
      `;
      }

      if (!indentationPropertyToCssMap[indentation]) {
        return '';
      }

      return `${indentationPropertyToCssMap[indentation]}: ${indentationValue}px;`;
    }).join('');
  };
};

const generateMarginRules = memoize(createIndentationRulesFunction('margin', MARGIN));
const generatePaddingRules = memoize(createIndentationRulesFunction('padding', PADDING));

export const withMargin = () => (WrappedComponent) => {
  const WithMargin = ({ ma, ...props }) => {
    const WrappedComponentWithMargin = styled(WrappedComponent)`
      ${generateMarginRules(ma)}
    `;

    return (
      <WrappedComponentWithMargin {...props} />
    );
  };

  const wrappedComponentName = WrappedComponent.displayName
    || WrappedComponent.name
    || 'Component';

  WithMargin.displayName = `withMargin(${wrappedComponentName})`;

  return WithMargin;
};

export const withPadding = () => (WrappedComponent) => {
  const WithPadding = ({ pa, ...props }) => {
    const WrappedComponentWithPadding = styled(WrappedComponent)`
      ${generatePaddingRules(pa)}
    `;

    return (
      <WrappedComponentWithPadding {...props} />
    );
  };

  const wrappedComponentName = WrappedComponent.displayName
    || WrappedComponent.name
    || 'Component';

  WithPadding.displayName = `withPadding(${wrappedComponentName})`;

  return WithPadding;
};
