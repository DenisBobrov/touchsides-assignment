import React from 'react';

import Icon from './Icon';

const iconFactory = icon => props => (
  <Icon
    icon={icon}
    {...props}
  />
);

export default iconFactory;
