import styled from 'styled-components';

const Form = styled.form`
  padding: 15px 10px;
  background-color: rgba(255, 255, 255, 0.5);
`;

export default Form;
