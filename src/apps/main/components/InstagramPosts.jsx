import React from 'react';
import isEmpty from 'lodash/isEmpty';

import { Container, Item } from 'components/Grid';
import { Heading } from 'components/Text';

import InstagramPost from './InstagramPost';

class InstagramPosts extends React.Component {
  render() {
    const { posts } = this.props;

    if (isEmpty(posts)) {
      return (
        <Heading as="h6" align="center">There are no data</Heading>
      );
    }

    return (
      <Container>
        {posts.map((post, index) => (
          <Item key={index} sm={6} md={4}>
            <InstagramPost post={post} />
          </Item>
        ))}
      </Container>
    );
  }
}

export default InstagramPosts;
