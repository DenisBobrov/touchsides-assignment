import pathToRegexp from 'path-to-regexp';

export const rootUrl = pathToRegexp.compile('/');
export const homeUrl = pathToRegexp.compile('/home');
export const contactUrl = pathToRegexp.compile('/contact');
export const instagramAccessToken = pathToRegexp.compile('/instagram/token');
