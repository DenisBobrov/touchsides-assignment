import React from 'react';
import styled from 'styled-components';

import colors from 'theme/colors';

const FormGroupWrapper = styled.div`
  width: 100%;
`;

const Error = styled.div`
  width: 100%
  color: ${colors.white};
  background-color: ${colors.gunmetal};
  padding: 10px 15px;
  padding-top: 0;
  box-sizing: border-box;
`;

const ErrorTriangle = styled.div`
  top: -10px;
  position: relative;
  width: 0;
  height: 0;
  border-style: solid;
  border-width: 0 7.5px 10px 7.5px;
  border-color: transparent transparent ${colors.gunmetal} transparent;
`;

class FormGroup extends React.Component {
  render() {
    const { children, error } = this.props;

    return (
      <FormGroupWrapper>
        <div>{children}</div>
        {error && (
          <Error>
            <ErrorTriangle />
            {error}
          </Error>
        )}
      </FormGroupWrapper>
    );
  }
}

export default FormGroup;
