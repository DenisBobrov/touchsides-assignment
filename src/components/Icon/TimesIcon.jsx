import { faTimes } from '@fortawesome/free-solid-svg-icons';

import iconFactory from './factory';

export default iconFactory(faTimes);
