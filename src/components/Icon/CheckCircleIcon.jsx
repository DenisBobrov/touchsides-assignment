import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

import iconFactory from './factory';

export default iconFactory(faCheckCircle);
