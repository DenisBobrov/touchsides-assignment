import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import colors from 'theme/colors';

export const textMixin = css`
  color: ${colors.white};
  font-size: 14px;
  text-decoration: none;
  text-align: ${({ align }) => align};
  width: ${({ fullWidth }) => (fullWidth ? '100%' : 'auto')};
`;

class Text extends React.Component {
  render() {
    const { as: As, className, children } = this.props;

    return (
      <As className={className}>
        {children}
      </As>
    );
  }
}

Text.propTypes = {
  as: PropTypes.oneOfType([PropTypes.string, PropTypes.func, PropTypes.object]),
  align: PropTypes.string,
  fullWidth: PropTypes.bool,
};

Text.defaultProps = {
  as: 'p',
  align: 'left',
  fullWidth: false,
};

export default styled(Text)`
  ${() => textMixin}
`;
