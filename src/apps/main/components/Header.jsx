import styled from 'styled-components';

import colors from 'theme/colors';

const Header = styled.header`
  background-color: ${colors.black};
`;

export default Header;
