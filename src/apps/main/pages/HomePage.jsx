import React from 'react';

import { getAccessToken } from 'services/instagram/client';
import { getAuthorizationUrl } from 'services/instagram/authorization';
import { getOwnerRecentMedia } from 'services/instagram/users';
import Link from 'components/Link';
import { Heading } from 'components/Text';

import InstagramPosts from '../components/InstagramPosts';

export default class HomePage extends React.Component {
  state = {
    loading: false,
    posts: [],
  };

  componentDidMount() {
    getAccessToken() && this.loadData();
  }

  async loadData() {
    this.setState({ loading: true });

    try {
      const response = await getOwnerRecentMedia();

      this.setState({ posts: response.data });
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    const { loading, posts } = this.state;

    if (!getAccessToken()) {
      return (
        <Link href={getAuthorizationUrl()}>Click here to login to instagram account</Link>
      );
    }

    if (loading) {
      return (
        <Heading as="h6" align="center">Loading...</Heading>
      );
    }

    return (
      <InstagramPosts posts={posts} />
    );
  }
}
