import React from 'react';
import isEmpty from 'lodash/isEmpty';
import { withFormik, Field } from 'formik';

import { isEmail, isPhone } from 'utils/validation';
import { Container, Item } from 'components/Grid';
import Button from 'components/Button';
import Form from 'components/Form';
import Message from 'components/Message';

import InputComponent from 'components/formik/InputComponent';

class ContactForm extends React.Component {
  render() {
    const {
      values, errors, submitCount, isSubmitting, handleSubmit,
    } = this.props;

    return (
      <React.Fragment>
        <Form onSubmit={handleSubmit} noValidate>
          {submitCount > 0 && !isEmpty(errors) && (
            <Message type="error" value="Please complete the form and try again" />
          )}
          <Container justifyContent="center">
            <Item sm={6} md={6}>
              <Field name="firstName" value={values.firstName} placeholder="First name" component={InputComponent} />
            </Item>
            <Item sm={6} md={6}>
              <Field name="lastName" value={values.lastName} placeholder="Last name" component={InputComponent} />
            </Item>
            <Item sm={6} md={6}>
              <Field name="email" type="email" value={values.email} placeholder="E-mail" component={InputComponent} />
            </Item>
            <Item sm={6} md={6}>
              <Field name="phone" value={values.phone} placeholder="Your phone number (optional)" component={InputComponent} />
            </Item>
            <Item>
              <Field name="message" value={values.message} placeholder="Your message..." as="textarea" component={InputComponent} />
            </Item>
            <Item md={6} justifyContent="center">
              <Button type="submit" disabled={isSubmitting}>Send</Button>
            </Item>
          </Container>
        </Form>
      </React.Fragment>
    );
  }
}

export default withFormik({
  enableReinitialize: true,
  mapPropsToValues: () => ({
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    message: '',
  }),
  validate: ({
    firstName, lastName, email, phone, message,
  }) => {
    const errors = {};

    !firstName && (errors.firstName = 'We need you first name.');
    !lastName && (errors.lastName = 'We need you last name.');
    !isEmail(email) && (errors.email = 'Please use a valid e-mail address.');
    !message && (errors.message = 'Sorry, you message can\'t be empty.');

    phone && !isPhone(phone) && (errors.phone = 'Please use a valid phone number.');

    return errors;
  },
  handleSubmit: async (values, { props, setSubmitting }) => {
    try {
      await props.onSubmit(values);
    } finally {
      setSubmitting(false);
    }
  },
})(ContactForm);
