import styled from 'styled-components';

import { MAX_COLUMNS_COUNT } from './Container';
import FlexContainer from './FLexContainer';
import Breakpoints from './breakpoints';

const getItemWidth = (columnsCount) => {
  const intColumnsCount = parseInt(columnsCount, 10);
  let finalColumnsCount = MAX_COLUMNS_COUNT;

  if (intColumnsCount && intColumnsCount < MAX_COLUMNS_COUNT && intColumnsCount > 0) {
    finalColumnsCount = intColumnsCount;
  }

  return `${finalColumnsCount / MAX_COLUMNS_COUNT * 100}%`;
};

const Item = styled(FlexContainer)`
  box-sizing: border-box;
  
  width: ${({ md }) => getItemWidth(md)};
  
  @media only screen and (max-width: ${Breakpoints.MD}px) {
    width: ${({ md }) => getItemWidth(md)};
  }
  
  @media only screen and (max-width: ${Breakpoints.SM}px) {
    width: ${({ sm }) => getItemWidth(sm)};
  }
  
  @media only screen and (max-width: ${Breakpoints.XS}px) {
    width: ${({ xs }) => getItemWidth(xs)};
  }
`;

Item.defaultProps = {
  xs: MAX_COLUMNS_COUNT,
  sm: MAX_COLUMNS_COUNT,
  md: MAX_COLUMNS_COUNT,
};

export default Item;
