import { faBars } from '@fortawesome/free-solid-svg-icons';

import iconFactory from './factory';

export default iconFactory(faBars);
