import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import MainApp from 'apps/main/MainApp';
import { rootUrl as mainRootUrl } from 'apps/main/urls';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path={mainRootUrl()} component={MainApp} />
        </Switch>
      </Router>
    );
  }
}

export default App;
