import { NavLink as RouterNavLink } from 'react-router-dom';
import styled from 'styled-components';

import colors from 'theme/colors';

import { textMixin } from '../Text';

const NavLink = styled(RouterNavLink)`
  ${() => textMixin}
    
  color: ${colors.bluishGrey};
`;

NavLink.defaultProps = {
  activeStyle: {
    color: colors.white,
  },
};

export default NavLink;
