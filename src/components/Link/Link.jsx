import styled from 'styled-components';

import { textMixin } from '../Text';

const Link = styled.a`
  ${() => textMixin}
  
  cursor: pointer;
`;

export default Link;
