import { faFacebookF } from '@fortawesome/free-brands-svg-icons';

import iconFactory from './factory';

export default iconFactory(faFacebookF);
