import styled from 'styled-components';

import colors from 'theme/colors';

const Input = styled.input`
  border: none;
  padding: 10px 15px;
  color: ${colors.black};
  width: ${({ fullWidth }) => (fullWidth ? '100%' : 'auto')};
  box-sizing: border-box;
`;

Input.defaultProps = {
  fullWidth: false,
};

export default Input;
