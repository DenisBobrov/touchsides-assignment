import { faTwitter } from '@fortawesome/free-brands-svg-icons';

import iconFactory from './factory';

export default iconFactory(faTwitter);
