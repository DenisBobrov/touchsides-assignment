import React from 'react';
import styled from 'styled-components';

import colors from 'theme/colors';
import FacebookFIcon from 'components/Icon/FacebookFIcon';
import TwitterIcon from 'components/Icon/TwitterIcon';
import InstagramIcon from 'components/Icon/InstagramIcon';
import Link from 'components/Link';

const FooterContent = styled.footer`
  min-height: 90px;
  background-color: ${colors.black};
  display: flex;
  align-items: center;
  justify-content: center;
  
  a {
    padding: 15px;
  }
`;

class Footer extends React.Component {
  render() {
    return (
      <FooterContent>
        <Link href="#" label="facebook">
          <FacebookFIcon />
        </Link>
        <Link href="#" label="twitter"><TwitterIcon /></Link>
        <Link href="#" label="instagram"><InstagramIcon /></Link>
      </FooterContent>
    );
  }
}

export default Footer;
