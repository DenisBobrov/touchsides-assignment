import styled from 'styled-components';

import colors from 'theme/colors';
import { textMixin } from 'components/Text';
import { withMargin } from 'enhancers/withIndentation';

const Button = styled.button`
  ${() => textMixin}
  background-color: ${colors.salmon};
  text-align: center;
  min-width: 120px;
  min-height: 34px;
  padding: none;
  margin: none;
  border: none;
  border-radius: 17px;
  cursor: pointer;
  
  :hover {
    filter: brightness(90%);
  }
  
  :active, :focus {
    outline: none;
  }
  
  :active {
    filter: brightness(80%);
  }
  
  :disabled {
    pointer-events: none;
    background-color: ${colors.warmGrey};
  }
`;

export default withMargin()(Button);
