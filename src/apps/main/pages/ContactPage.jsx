import React from 'react';

import { Container, Item } from 'components/Grid';
import { Heading } from 'components/Text';
import Message from 'components/Message';

import ContactForm from '../components/ContactForm';

export default class ContactPage extends React.Component {
  state = {
    submitted: false,
  };

  handleSubmit = values => new Promise((resolve) => {
    setTimeout(() => {
      this.setState({ submitted: true }, resolve);
    }, 2000);
  });

  render() {
    const { submitted } = this.state;

    return (
      <Container alignItems="center" direction="column">
        <Item ma="t-double b" md={4} sm={6} xs={8} justifyContent="center">
          <Heading as="h4" align="center">We would love hear from you</Heading>
        </Item>
        <Item md={8} sm={8} justifyContent="center">
          {submitted ? (
            <Message type="success" value="Thank you, we have received your message" />
          ) : (
            <ContactForm onSubmit={this.handleSubmit} />
          )}
        </Item>
      </Container>
    );
  }
}
