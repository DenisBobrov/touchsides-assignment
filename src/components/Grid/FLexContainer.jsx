import styled from 'styled-components';

const FlexContainer = styled.div`
  display: flex;
  justify-content: ${({ justifyContent }) => justifyContent}
`;

FlexContainer.defaultProps = {
  justifyContent: 'flex-start',
};

export default FlexContainer;
