import React from 'react';
import queryString from 'query-string';
import { Redirect } from 'react-router-dom';

import { storeAccessToken } from 'services/instagram/client';

import { homeUrl } from '../urls';

class InstagramAccessTokenPage extends React.Component {
  constructor(props) {
    super(props);

    const hash = queryString.parse(props.location.hash);

    storeAccessToken(hash.access_token);
  }

  render() {
    return (
      <Redirect to={homeUrl()} />
    );
  }
}

export default InstagramAccessTokenPage;
