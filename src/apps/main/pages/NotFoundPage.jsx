import React from 'react';

import { Heading } from 'components/Text';

class NotFoundPage extends React.Component {
  render() {
    return (
      <Heading as="h4" align="center">Opp! Page not found</Heading>
    );
  }
}

export default NotFoundPage;
