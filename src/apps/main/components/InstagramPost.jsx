import React from 'react';
import styled from 'styled-components';

import colors from 'theme/colors';

import Text from 'components/Text';
import { withPadding } from 'enhancers/withIndentation';

const Wrapper = styled.div`
  width: 320px;
  background-color: ${({ post }) => (post.caption ? colors.white : 'transparent')};
  
  ${Text} {
    color: ${colors.black};
  }
`;

const Image = styled.img`
  width: 100%;
`;

const CaptionWrapper = withPadding()('div');

class InstagramPost extends React.Component {
  render() {
    const { caption, images: { low_resolution: { url } } } = this.props.post;

    return (
      <Wrapper {...this.props}>
        <Image src={url} />
        {caption && (
          <CaptionWrapper pa>
            <Text>{caption.text}</Text>
          </CaptionWrapper>
        )}
      </Wrapper>
    );
  }
}

export default InstagramPost;
